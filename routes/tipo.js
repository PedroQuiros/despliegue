/*****************Archivo tipo***************
 * 
 * Archivo del router para listar los tipos de inmuebles
 * 
 * Autor: Pedro A. Muñoz
 */

const express = require('express');
const Tipo = require('../models/tipo');

let router = express.Router();

// Mostrar todos los tipos de inmuebles
router.get('/', (req, res) => {
    Tipo.find().then(resultado => {
        res.render('tipos_inmueble', {error: false, tipos: resultado});
    }).catch (error => {
        res.render('tipos_inmueble', {error: true});
    }); 
});

module.exports = router;