/*****************Archivo index***************
 * 
 * Archivo del router gestionar las vistas principales
 * 
 * Autor: Pedro A. Muñoz
 */

const express = require('express');
const Tipo = require('../models/tipo');
const passport = require('passport');
const {Strategy, ExtractJwt} = require('passport-jwt');
const jwt = require('jsonwebtoken');

let router = express.Router();

// Index o página principal
router.get('/',(req, res) => {
    res.render('index',{error: null, delete: 'casa'});
});

// Función para devolver error si el usuario no esta logueado e intenta entrar en zona protegida
router.get('/prohibido',(req, res) => {
    res.send({error: true, mensajeError: "prohibido"});
});

// Función para devolver error si el usuario no esta logueado e intenta entrar en zona protegida
router.delete('/prohibido',(req, res) => {
    console.log("Prohibido 2b");
    res.send({error: true, mensajeError: "prohibido"});
});

// Mostrar pantalla de introducción de inmuebles
router.get('/nuevo_inmueble', (req, res) => {
    Tipo.find().then(resultado => {
        res.render('nuevo_inmueble',{error: false, tipos: resultado});
    }).catch (error => {
        res.render('nuevo_inmueble',{error: true, tipos: resultado});
    });
    
});

// Mostrar pantalla de filtrar inmuebles
router.get('/filtrar_inmuebles', (req, res) => {
    res.render('filtrar_inmuebles');
});

// Mostrar pantalla login
router.get('/login', (req, res) => {
    res.render('login');
});

// Mostrar pantalla registro
router.get('/registro', (req, res) => {
    res.render('registro');
});

module.exports = router;