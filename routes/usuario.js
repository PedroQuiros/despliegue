/*****************Archivo usuario***************
 * 
 * Archivo del router para gestionar las funciones de crear y validar
 *  usuarios de la base de datos inmuebles 
 * 
 * Autor: Pedro A. Muñoz
 */

const express = require('express');
const Usuario = require('../models/usuario');
const md5 = require('md5');
const jwt = require('jsonwebtoken');

const secreto = "secretoDAW";

let router = express.Router();

// Función para realizar el login a la web
router.post('/login', (req, res) => {
    Usuario.findOne({login: req.body.login, password: md5(req.body.password)}).then(resultado => {
        if (resultado == null){
            res.send({error: true, mensajeError: "Usuario incorrecto"});
        }
        else{
            let token = jwt.sign({login: req.body.login}, secreto, {expiresIn:"30 minutes"}); // Generamos un token de 30 minutos
            res.send({error: false, token: token});
        }

    }).catch(error => {
        res.send({error: true, mensajeError: "Usuario incorrecto"});
    });
});

// Función para registrar un usuario
router.post('/registro', (req, res) => {
    let nuevoUsuario = new Usuario({
        nombre: req.body.nombre,
        login: req.body.login,
        password: md5(req.body.password),
    });
    nuevoUsuario.save().then(resultado => {
        res.render('login', {error: false}); 
    }).catch(error => {
        res.render('registro', {error: true});
    });
});

module.exports = router;