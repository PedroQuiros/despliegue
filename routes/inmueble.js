/*****************Archivo inmuelble***************
 * 
 * Archivo del router para gestionar las funciones de listar, crear y borrar
 *  inmuebles de la base de datos inmuebles 
 * 
 * Autor: Pedro A. Muñoz
 */

const express = require('express');
const fileUpload = require('express-fileupload');
const fs = require('fs');
const rimraf = require("rimraf");
const Inmueble = require('../models/inmueble');
const Tipo = require('../models/tipo');
const passport = require('passport');
const {Strategy, ExtractJwt} = require('passport-jwt');
const jwt = require('jsonwebtoken');

let router = express.Router();

// Obtener todos los inmuebles 
router.get('/', (req, res) => {
    Inmueble.find().then(resultado => {
        res.render('lista_inmuebles', {error: false, inmuebles: resultado});
    }).catch (error => {
        res.render('lista_inmuebles', {error: true});
    }); 
});

// Obtener los inmuebles de un mismo tipo
router.get('/tipo/:tipo', (req, res) => {
    let inmuebles;
    Inmueble.find({"tipo":req.params.tipo}).then(resultado => {
        inmuebles = resultado;
        Tipo.findById(req.params.tipo).then(resultado => {
            res.render('lista_inmuebles', {error: false, tipo: resultado, inmuebles: inmuebles});
        }).catch(error => {
            res.render('lista_inmuebles', {error: true});
        });
    }).catch(error => {
        res.render('lista_inmuebles', {error: true});
    });
});

// Obtener inmuebles con un filtro de precio, superficie y habitaciones.
router.post('/filtro', (req, res) => {
    let precio = req.body.precio;
    let superficie = req.body.superficie;
    let habitaciones = req.body.habitaciones;
    if (precio == ''){
        precio = 99999999;
    }
    if (superficie == ''){
        superficie = 0;
    }
    if (habitaciones == ''){
        habitaciones = 0;
    }
    Inmueble.find({$and:[{"precio":{$lte:precio}},{"superficie":{$gte:superficie}},{"habitaciones":{$gte:habitaciones}}]}).then(resultado => {
        res.render('lista_inmuebles', {error: false, inmuebles: resultado}); 
    }).catch(error => {
        res.render('lista_inmuebles', {error: true, inmuebles: []});
    }); 
});

// Obtener inmueble por su id
router.get('/:id', (req, res) => {
    let inmueble;
    Inmueble.findById(req.params.id).then(resultado => {
        inmueble = resultado;
        Tipo.findById(inmueble.tipo).then(resultado => {
            res.render('ficha_inmueble', {error: false, tipo: resultado, inmueble: inmueble});
        }).catch(error => {
            res.render('ficha_inmueble', {error: true});
        });
    }).catch(error => {
        res.render('ficha_inmueble', {error: true});
    });
});

// Crear inmueble
router.post('/', passport.authenticate('jwt', {session: false, failureRedirect:'/prohibido'}), (req, res) => {
    let nuevoInmueble = new Inmueble({
        descripcion: req.body.descripcion,
        tipo: req.body.tipo,
        habitaciones: req.body.habitaciones,
        superficie: req.body.superficie,
        precio: req.body.precio,
        imagen: req.files.imagen.name
    });
    nuevoInmueble.save().then(resultado => {
        fs.mkdir('/home/despliegue/ProyectosNode/Practicas/Practica3/public/uploads/'+ nuevoInmueble.id, function(err) {
            if (err)
                res.send({error: true, mensajeError: "No se ha creado el inmueble"});
            else {
                let imagen = req.files.imagen;
                imagen.mv('/home/despliegue/ProyectosNode/Practicas/Practica3/public/uploads/'+ nuevoInmueble.id + "/" + req.files.imagen.name, function(err) {
                    if (err)
                        res.send({error: true, mensajeError: "No se ha creado el inmueble"});
                    else
                        res.send({error: false, mensajeError: "Se ha creado el inmueble"});
                });
            }
        });  
    }).catch(error => {
        res.send({error: true, mensajeError: "No se ha creado el inmueble"});
    });
});

// Borrar inmueble
router.delete('/:id', passport.authenticate('jwt', {session: false, failureRedirect:'/prohibido'}), (req, res) => {
    Inmueble.findByIdAndRemove(req.params.id).then(resultado => {
        rimraf('/home/despliegue/ProyectosNode/Practicas/Practica3/public/uploads/'+ req.params.id, function(err) {
            if (err)
                res.send({error: true, mensajeError: "No se ha eliminado el inmueble"});
            else
                res.send({error: false, mensajeError: "Se ha eliminado el inmueble"});
        }); 
    }).catch(error => {
        res.send({error: true, mensajeError: "No se ha eliminado el inmueble"});
    }); 
});

module.exports = router;