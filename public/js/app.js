function enviarInmueble() {
    var formData = new FormData(document.getElementById('nuevoInmueble'));
    $.ajax({
        url:"/inmuebles",
        type:"POST",
        data: formData,
        contentType:false,
        cache:false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+ localStorage.getItem('token'));
        },
        success: function(data) {
                if (data.error){
                    if (data.mensajeError == 'prohibido'){
                        alert('Acceso no permitido');
                        document.location = '/login';
                    }
                    else{
                        alert('Error añadiendo el inmueble');
                        document.location = '/nuevo_inmueble';
                    }  
                }    
                else{
                    alert('Inmueble añadido correctamente');
                    document.location = '/';
                }                     
               
        }
    });
}

function borrarInmueble(id) {
    var formData = new FormData(document.getElementById('deleteInmueble'));
    $.ajax({
        url:"/inmuebles/" + id,
        type:"DELETE",
        data: formData,
        contentType:false,
        cache:false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+ localStorage.getItem('token'));
        },
        success: function(data) {
            if (data.error){
                if (data.mensajeError == 'prohibido'){
                    alert('Acceso no permitido');
                    document.location = '/login';
                }
                else{
                    alert('Error borrando el inmueble');
                    document.location = '/inmuebles';
                }
            }    
            else{
                alert('Inmueble borrado correctamente');
                document.location = '/';
            }  
        }
    });
}

function login()
{
    $.ajax({
        url:"/usuarios/login",
        type:"POST",
        data: JSON.stringify({login: $("#login").val(), password: $("#password").val(), edad: $("#edad").val()}),
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success: function(data) {
            if (data.error) {
                alert("Usuario incorrecto");
                document.location = '/login';
            } else {
                localStorage.setItem("token", data.token);
                document.location = '/';
            }
        }
    });
}