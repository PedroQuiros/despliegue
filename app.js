/**********Programa principal****************
 * 
 * Programa para administrar y/o gestionar inmuebles
 * 
 * Autor: Pedro A. Muñoz
*/

const express = require('express');
const fileUpload = require('express-fileupload');
const mongoose = require('mongoose');
const index = require('./routes/index');
const inmuebles = require('./routes/inmueble');
const tipos= require('./routes/tipo');
const usuarios= require('./routes/usuario');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const passport = require('passport');
const {Strategy, ExtractJwt} = require('passport-jwt');
const jwt = require('jsonwebtoken');

const secreto = "secretoDAW";

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/inmuebles');

passport.use(new Strategy({secretOrKey: secreto, jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()}, (payload, done) => {
    if (payload.login) {
        return done(null, {login: payload.login});
    } 
    else {
            return done(new Error("Usuario incorrecto"), null);
        }
}));

let app = express();

app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use('/public', express.static('./public'));

 app.use((req, res, next) => {
    res.locals.error = null;
    res.locals.login = null;
    next();
 });  

app.use(fileUpload());
app.use(methodOverride('_method'));

app.use('/', index);
app.use('/inmuebles', inmuebles);
app.use('/tipos', tipos);
app.use('/usuarios', usuarios);

app.listen(8080); 