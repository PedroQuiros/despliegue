const mongoose = require('mongoose');
let usuarioSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    login: {
        type: String,
        required: true,
        unique: true,
        minlength: 1,
        trim: true
    },
    password: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    }
});
let Usuario = mongoose.model('usuario', usuarioSchema);
module.exports = Usuario;