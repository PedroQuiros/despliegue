/*****************Archivo tipo***************
 * 
 * Archivo del modelo para gestionar las 
 *  propiedades de los tipos de inmuebles 
 * 
 * Autor: Pedro A. Muñoz
 */

const mongoose = require('mongoose');

let tipoSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true
    }
});

let Tipo = mongoose.model('tipo', tipoSchema);

module.exports = Tipo;